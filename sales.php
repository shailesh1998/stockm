<?php
include('inc/config.php');
#include('inc/session_check.php');

?>

<!doctype html>
<html lang="en">
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <!-- Header include start --->
    <?php include('inc/header.php'); ?>
    <!-- Header include End --->
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>

    <script type="text/javascript">
        // $(function () {
        //     $('#alertMessage').delay(2000).fadeOut();
        // });

        function showAlert(){
            if($("#myAlert").find("div#myAlert2").length==0){
                $("#myAlert").append("<div class='alert alert-success alert-dismissable' id='myAlert2'> <button type='button' class='close' data-dismiss='alert'  aria-hidden='true'>&times;</button> Success! message sent successfully.</div>");
            }
            $("#myAlert").css("display", "");
        }
    </script>

</head>

<body>
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
    <div class="app-header header-shadow">

        <!-- Header include start --->
        <?php include('inc/topbar.php'); ?>
        <!-- Header include End --->

    </div>
    <div class="ui-theme-settings">
        <!-- theme setting include start --->
        <?php include('inc/theme_settings.php'); ?>
        <!-- theme setting include End --->
    </div>
    <div class="app-main">

        <!-- sidebar include start --->
        <?php include('inc/sidebar.php') ?>
        <!-- sidebar include End --->

        <div class="app-main__outer">
            <div class="app-main__inner">
                <div class="app-page-title">
                    <div class="page-title-wrapper">
                        <div class="page-title-heading">
                            <div class="page-title-icon">
                                <i class="pe-7s-car icon-gradient bg-mean-fruit">
                                </i>
                            </div>
                            <div>Sales Manage
                                <div class="page-title-subheading">Sales Manage ADD EDIT INSERT UPDATE
                                </div>
                            </div>
                        </div>
                        <div class="page-title-actions">

                            <div class="d-inline-block dropdown">
                                <button type="button" data-toggle="dropdown" aria-haspopup="true"
                                        aria-expanded="false" class="btn-shadow dropdown-toggle btn btn-info">
                                        <span class="btn-icon-wrapper pr-2 opacity-7">

                                            <i class="fas fa-plus-circle fa-w-20"></i>
                                        </span>
                                    Add
                                </button>
                                <div tabindex="-1" role="menu" aria-hidden="true"
                                     class="dropdown-menu dropdown-menu-right">
                                    <ul class="nav flex-column">
                                        <li class="nav-item">
                                            <a href=" " class="nav-link">
                                                <i class="nav-link-icon lnr-inbox"></i>
                                                <span>
                                                        Invoice Genrate
                                                    </span>
                                                <div class="ml-auto badge badge-pill badge-secondary">86</div>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="add_client.php" class="nav-link">
                                                <i class="nav-link-icon lnr-book"></i>
                                                <span>
                                                        Add Client
                                                    </span>

                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="transection.php" class="nav-link">
                                                <i class="nav-link-icon lnr-picture"></i>
                                                <span>
                                                        View Transection
                                                    </span>
                                            </a>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">

                    </div>
                    <div class="col-md-6 col-xl-4">
                        <div class="card mb-3 widget-content bg-midnight-bloom">
                            <div class="widget-content-wrapper text-white">
                                <div class="widget-content-left">
                                    <div class="widget-heading">Total Orders</div>
                                    <div class="widget-subheading">Last year expenses</div>
                                </div>
                                <div class="widget-content-right">
                                    <div class="widget-numbers text-white"><span>1896</span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-xl-4">
                        <div class="card mb-3 widget-content bg-arielle-smile">
                            <div class="widget-content-wrapper text-white">
                                <div class="widget-content-left">
                                    <div class="widget-heading">Clients</div>
                                    <div class="widget-subheading">Total Clients Profit</div>
                                </div>
                                <div class="widget-content-right">
                                    <?php
                                    $sql = "SELECT * FROM tbl_client";
                                    $res = $conn->query($sql);
                                    ?>
                                    <div class="widget-numbers text-white">
                                        <span><?php echo $msgCount = mysqli_num_rows($res); ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-xl-4">
                        <div class="card mb-3 widget-content bg-grow-early">
                            <div class="widget-content-wrapper text-white">
                                <div class="widget-content-left">
                                    <div class="widget-heading">Followers</div>
                                    <div class="widget-subheading">People Interested</div>
                                </div>
                                <div class="widget-content-right">
                                    <div class="widget-numbers text-white"><span>46%</span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-xl-none d-lg-block col-md-6 col-xl-4">
                        <div class="card mb-3 widget-content bg-premium-dark">
                            <div class="widget-content-wrapper text-white">
                                <div class="widget-content-left">
                                    <div class="widget-heading">Products Sold</div>
                                    <div class="widget-subheading">Revenue streams</div>
                                </div>
                                <div class="widget-content-right">
                                    <div class="widget-numbers text-warning"><span>$14M</span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">

                    <div class="col-md-12">
                        <div class="main-card mb-3 card">
                            <div class="card-body">
                                <h5 class="card-title">Add Sales</h5>
                                <form action="sales-process.php" method='post' class="needs-validation" novalidate>
                                    <div class="form-row">
                                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                            <label class="btn btn-secondary active">
                                                <input type="radio" name="options" id="option1" autocomplete="off"
                                                       checked>Creadit
                                            </label>
                                            <label class="btn btn-secondary">
                                                <input type="radio" name="options" id="option2" autocomplete="off">
                                                Cash
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col-md-4 mb-3"><br>
                                            <label for="validationCustom01">Invoice Number:</label>
                                            <input type="text" name="invoice_number" class="form-control"
                                                   id="validationCustom01" placeholder="Invoice number" value=""
                                                   required disabled>
                                        </div>
                                        <div class="col-md-4 mb-3">
                                        </div>
                                        <div class="col-md-4 mb-3">
                                            <label for="validationCustomUsername">Date</label>
                                            <div class="input-group">
                                                <input type="Date" name="date" class="form-control" id=""
                                                       placeholder="" aria-describedby="inputGroupPrepend" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustom03">Client Name</label>
                                            <input type="text" name="client_name" class="form-control"
                                                   id="validationCustom03" placeholder="Client Name" required>
                                            <div class="invalid-feedback">
                                                Enter Client Name!
                                            </div>
                                        </div>

                                        <div class="col-md-6 mb-3">
                                        </div>
                                    </div>
                                    <!----------------------------------------------------------------------------------------------->
                                    <form id="fupForm" name="form1" method="post">
                                        <div class="row">
                                            <div class="col-md-3 mb-3">
                                                <div class="form-group">
                                                    <!-- <label for="email">Name:</label> -->
                                                    <input type="text" class="form-control" id="product_name"
                                                           placeholder="Product Name"
                                                           name="product_name">
                                                </div>
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <div class="form-group">
                                                    <!-- <label for="pwd">Email:</label> -->
                                                    <input type="email" class="form-control" id="product_qty"
                                                           placeholder="Quantity" name="product_qty">
                                                </div>
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <div class="form-group">
                                                    <!-- <label for="pwd">Phone:</label> -->
                                                    <input type="text" class="form-control" id="product_prise"
                                                           placeholder="Price"
                                                           name="product_prise">
                                                </div>
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <div class="form-group">
                                                    <input type="button" name="save" onclick="showAlert();" class="btn btn-primary"
                                                           value="Save to database" id="butsave">
                                                </div>
                                            </div>
                                        </div>
                                        <?php //
                                        //echo $notification; ?>
                                    </form>


                                    <div class="row">
                                        <div class="col-lg-12">
                                <div class="main-card mb-3 card">
                                    <div class="card-body">
                                        <h5 class="card-title">Total Added Product</h5>
                                        
                                        <table class="mb-0 table table-hover">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Product Name</th>
                                                <th>Quantity</th>
                                                <th>Prise</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                             <?php
                                                                                                          
                                               
                                                $res = mysqli_query($conn,"SELECT * FROM tbl_temp_product");
                                                $result = array();
                                                 
                                                while( $row = mysqli_fetch_array($res) )
                                                    array_push($result, array(
                                                                            'product_name' => $row[0],
                                                                            'product_qty'  => $row[1],
                                                                            'company' => $row[2]
                                                                        )
                                                    );
                                                 
                                                echo json_encode(array("result" => $result));
                                                ?>
                                                
                                            <?php
                                                $sr=1;                                           
                                                $record=mysqli_query($conn,"SELECT * FROM tbl_temp_product");
                                                while($data=mysqli_fetch_array($record)){
                                            ?>
                                            <tr>
                                                <th scope="row"><?php echo $sr; ?></th>
                                                <td><?php echo $data['product_name']; ?></td>
                                                <td><?php echo $data['product_qty']; ?></td>
                                                <td><?php echo $data['product_price']; ?></td>
                                            </tr> 
                                            <?php
                                                $sr++;
                                                }
                                            ?>                                           
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                                    </div>
                                    <!----------------------------------------------------------------------------------------------->
                                    <div class="form-row">
                                        <div class="col-md-4 mb-3">
                                            <label for="validationCustom03">Total Amount</label>
                                            <input type="number" name="total_payment" class="form-control"
                                                   id="validationCustom03" placeholder="Total Amount" required>
                                            <div class="invalid-feedback">
                                                Enter Valid Amount!
                                            </div>
                                        </div>

                                        <div class="col-md-4 mb-3">
                                            <label for="validationCustom03">Received Amount</label>
                                            <input type="number" name="payment_receive" class="form-control"
                                                   id="validationCustom03" placeholder="Received Amount" required>
                                            <div class="invalid-feedback">
                                                Enter Received Amount!
                                            </div>
                                        </div>

                                        <div class="col-md-4 mb-3">
                                            <label for="validationCustom03">Balanced Due</label>
                                            <input type="number" name="payment_due" class="form-control"
                                                   id="validationCustom03" placeholder="Balaced Due" required>
                                            <div class="invalid-feedback">
                                                Enter Balanced Due!
                                            </div>
                                        </div>

                                    </div>

                                    <div class="form-row">
                                        <div class="col-md-4 mb-3">
                                            <label for="validationCustom03">Payment Type</label>
                                            <select name="payment_type" id="exampleSelect" class="form-control">
                                                <option>Cash</option>
                                                <option>Chaque</option>
                                                <option>Bank Transfer</option>
                                            </select>
                                        </div>
                                        <div class="col-md-8 mb-3">
                                            <label for="exampleText" class="">Description</label>
                                            <textarea name="description" id="exampleText" class="form-control"
                                                      placeholder="Add Description" rows="2"></textarea>
                                        </div>

                                    </div>

                                    <div class="position-relative form-group">
                                        <div class="position-relative form-group"></div>
                                        <small class="form-text text-muted">add sales for some cient who have
                                            multiorder and make ony one invoce for week</small>
                                    </div>
                                    <input Type="submit" name="submit" class="mt-1 btn btn-primary"
                                           value="Add Sale">
                                    <!-- <button class="mt-1 btn btn-primary">Submit</button> -->
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include('js.php'); ?>
    </div>
</div>
<!-- script is here -->
<script>
    $(document).ready(function () {
        $('#butsave').on('click', function () {
            var product_name = $('#product_name').val();
            var product_qty = $('#product_qty').val();
            var product_prise = $('#product_prise').val();          

            if (product_name != "" && product_qty != "" && product_prise != "") {
                $.ajax({
                    url: "test-2.php",
                    type: "POST",
                    data: {
                        name: product_name,
                        qty: product_qty,
                        price: product_prise,
                    },
                    cache: false,
                    success: function (dataResult) {
                        var dataResult = JSON.parse(dataResult);
                        if (dataResult.statusCode == 200) {
                            $("#butsave").removeAttr("disabled");
                            $('#fupForm').find('input:text').val('');
                            $("#success").show();
                            $('#success').html('Data added successfully !');
                        } else if (dataResult.statusCode == 201) {
                            alert("Error occured !");
                        }
                    }
                });
            } else {
                alert('Please fill all the field !');
            }
        });
    });

</script>
</body>
</html>