<?php
    include('inc/config.php');
    #include('inc/session_check.php');     
?>

<!doctype html>
<html lang="en">

<head>
    <!-- Header include start --->
    <?php include('inc/header.php'); ?>
    <!-- Header include End --->

    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

    <script type="text/javascript">
    $(function() {
        $('#alertMessage').delay(2000).fadeOut();
    });
    </script>

    <style>
    .card.mb-3 {
        margin-bottom: 10px !important;
    }

    .card-title {
        padding-top: 10px !important;
    }
    </style>


</head>

<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        <div class="app-header header-shadow">

            <!-- Header include start --->
            <?php include('inc/topbar.php'); ?>
            <!-- Header include End --->

        </div>
        <div class="ui-theme-settings">
            <!-- theme setting include start --->
            <?php include('inc/theme_settings.php'); ?>
            <!-- theme setting include End --->
        </div>
        <div class="app-main">

            <!-- sidebar include start --->
            <?php include('inc/sidebar.php') ?>
            <!-- sidebar include End --->

            <div class="app-main__outer">
                <div class="app-main__inner">
                    <div class="app-page-title">
                        <div class="page-title-wrapper">
                            <div class="page-title-heading">
                                <div class="page-title-icon">
                                    <i class="pe-7s-car icon-gradient bg-mean-fruit">
                                    </i>
                                </div>
                                <div>Transection
                                    <div class="page-title-subheading">All transection seen by
                                    </div>

                                </div>
                            </div>
                            <div class="page-title-actions">

                                <div class="d-inline-block dropdown">
                                    <button type="button" data-toggle="dropdown" aria-haspopup="true"
                                        aria-expanded="false" class="btn-shadow dropdown-toggle btn btn-info">
                                        <span class="btn-icon-wrapper pr-2 opacity-7">

                                            <i class="fas fa-plus-circle fa-w-20"></i>
                                        </span>
                                        Add
                                    </button>
                                    <div tabindex="-1" role="menu" aria-hidden="true"
                                        class="dropdown-menu dropdown-menu-right">
                                        <ul class="nav flex-column">
                                            <li class="nav-item">
                                                <a href=" " class="nav-link">
                                                    <i class="nav-link-icon lnr-inbox"></i>
                                                    <span>
                                                        Invoice Genrate
                                                    </span>
                                                    <div class="ml-auto badge badge-pill badge-secondary">86</div>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="sales.php" class="nav-link">
                                                    <i class="nav-link-icon lnr-book"></i>
                                                    <span>
                                                        Add New Sale
                                                    </span>

                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="javascript:void(0);" class="nav-link">
                                                    <i class="nav-link-icon lnr-picture"></i>
                                                    <span>
                                                        View Transection
                                                    </span>
                                                </a>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <?php
                            $record=mysqli_query($conn,"SELECT * FROM tbl_transection ORDER BY t_id DESC");
                            while($data=mysqli_fetch_array($record))
                            {
                        ?>
                        <div class="col-md-12">
                            <div class="mb-3 text-left card card-body">
                                <a href="transection_profile.php">
                                    <div class="row">
                                        <div class="col-md-9">
                                            <h5 class="card-title">Client name: <?php $data['c_first_name']; ?></h5>
                                        </div>
                                        <div class="col-md-3">
                                            <span class=" card-title text-right">Date: 20/12/2020</span>
                                        </div>
                                    </div>
                                </a>                                
                                <div class="row">
                                    <div class="col-md-3">
                                        <h5 class="card-title">Last Transection: 23000</h5>
                                    </div>
                                    <div class="col-md-6">
                                        <h5 class="card-title">Total: 45000</h5>
                                    </div>
                                    <div class="col-md-3">
                                        <a href="transection_profile.php">
                                            <button class="btn btn-outline-focus">See All Transection</button>
                                        </a>
                                    </div>
                                </div>                        
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>


            </div>


        </div>
        <?php include('js.php'); ?>
    </div>
    </div>

</body>

</html>