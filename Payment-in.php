<?php
    include('inc/config.php');
    #include('inc/session_check.php');     
?>

<!doctype html>
<html lang="en">

<head>
    <!-- Header include start --->
    <?php include('inc/header.php'); ?>
    <!-- Header include End --->

    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

    <script type="text/javascript">
    $(function() {
        $('#alertMessage').delay(2000).fadeOut();
    });
    </script>

   

</head>

<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        <div class="app-header header-shadow">

            <!-- Header include start --->
            <?php include('inc/topbar.php'); ?>
            <!-- Header include End --->

        </div>
        <div class="ui-theme-settings">
            <!-- theme setting include start --->
            <?php include('inc/theme_settings.php'); ?>
            <!-- theme setting include End --->
        </div>
        <div class="app-main">

            <!-- sidebar include start --->
            <?php include('inc/sidebar.php') ?>
            <!-- sidebar include End --->

            <div class="app-main__outer">
                <div class="app-main__inner">
                    <div class="app-page-title">
                        <div class="page-title-wrapper">
                            <div class="page-title-heading">
                                <div class="page-title-icon">
                                    <i class="pe-7s-car icon-gradient bg-mean-fruit">
                                    </i>
                                </div>
                                <div>Payment In
                                    <div class="page-title-subheading">Payment Entery page here!
                                    </div>

                                </div>
                            </div>
                            <div class="page-title-actions">

                                <div class="d-inline-block dropdown">
                                    <button type="button" data-toggle="dropdown" aria-haspopup="true"
                                        aria-expanded="false" class="btn-shadow dropdown-toggle btn btn-info">
                                        <span class="btn-icon-wrapper pr-2 opacity-7">

                                            <i class="fas fa-plus-circle fa-w-20"></i>
                                        </span>
                                        Add
                                    </button>
                                    <div tabindex="-1" role="menu" aria-hidden="true"
                                        class="dropdown-menu dropdown-menu-right">
                                        <ul class="nav flex-column">
                                            <li class="nav-item">
                                                <a href=" " class="nav-link">
                                                    <i class="nav-link-icon lnr-inbox"></i>
                                                    <span>
                                                        Invoice Genrate
                                                    </span>
                                                    <div class="ml-auto badge badge-pill badge-secondary">86</div>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="sales.php" class="nav-link">
                                                    <i class="nav-link-icon lnr-book"></i>
                                                    <span>
                                                        Add New Sale
                                                    </span>

                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="javascript:void(0);" class="nav-link">
                                                    <i class="nav-link-icon lnr-picture"></i>
                                                    <span>
                                                        View Transection
                                                    </span>
                                                </a>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                              
                       
                        
                    <div class="row">

                        <div class="col-md-12">
                            <div class="main-card mb-3 card">
                                <div class="card-body">
                                    <h5 class="card-title">Payment In</h5>

                                    <!--------------------------------------------------------------------------------------------------->

                                    <form class="needs-validation" novalidate>
                                        
                                        <div class="form-row">
                                            <div class="col-md-4 mb-3">
                                                <label for="validationCustom01">Receipt Number:</label>
                                                <div class="input-group">
                                                <input type="number" name="invoice_number" class="form-control" id="validationCustom01"
                                                    placeholder="Receipt number" value="" required >
                                                </div>

                                            </div>
                                            <div class="col-md-4 mb-3"></div>
                                           
                                            <div class="col-md-4 mb-3">
                                                <label for="validationCustom01">Date</label>
                                                <div class="input-group">
                                                    <input type="Date" name="date" class="form-control" id="" placeholder=""aria-describedby="inputGroupPrepend" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-6 mb-3">
                                                <label for="validationCustom03">Client Name</label>
                                                <input type="text" name="client_name" class="form-control" id="validationCustom03"
                                                    placeholder="Client Name" required>
                                                <div class="invalid-feedback">
                                                    Enter Client Name!
                                                </div>
                                            </div>

                                            <div class="col-md-6 mb-3">
                                            </div>
                                        </div>
   
                                        <div class="form-row">

                                            <div class="col-md-6 mb-3">
                                                <label for="validationCustom03">Received Amount</label>
                                                <input type="number" name="recived_payment" class="form-control" id="validationCustom03"placeholder="Total Amount" required>
                                                <div class="invalid-feedback">
                                                    Enter Valid Amount!
                                                </div>
                                            </div>
                                            <div class="col-md-6 mb-3">
                                            </div>
                                            
                                        </div>

                                        <div class="form-row">

                                            <div class="col-md-6 mb-3">
                                                <label for="validationCustom03">Payment Type</label>
                                                <select name="payment_type" id="exampleSelect" class="form-control">
                                                    <option>Cash</option>
                                                    <option>Chaque</option>
                                                    <option>Bank Transfer</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-6 mb-3">
                                                <label for="exampleText" class="">Description</label>
                                                <textarea name="desc" id="exampleText" class="form-control"
                                                    placeholder="Add Description" rows="2"></textarea>
                                            </div>

                                        </div>

                                        <div class="position-relative form-group">
                                            <div class="position-relative form-group"></div>
                                            <small class="form-text text-muted">add sales for some cient who have
                                                multiorder and make ony one invoce for week</small>
                                        </div>
                                        <button class="mt-1 btn btn-primary">Payment In</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

            s
                </div>


            </div>
            <?php include('js.php'); ?>
        </div>
    </div>

</body>

</html>