<div class="app-sidebar sidebar-shadow">
    <div class="app-header__logo">
        <div class="logo-src"></div>
        <div class="header__pane ml-auto">
            <div>
                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic"
                    data-class="closed-sidebar">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
    <div class="app-header__mobile-menu">
        <div>
            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
        </div>
    </div>
    <div class="app-header__menu">
        <span>
            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                <span class="btn-icon-wrapper">
                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                </span>
            </button>
        </span>
    </div>
    <div class="scrollbar-sidebar">
        <div class="app-sidebar__inner">
            <ul class="vertical-nav-menu">
                <li class="app-sidebar__heading">Dashboards</li>
                <li>
                    <a href="index.php" class="mm-active">
                        <i class="metismenu-icon pe-7s-rocket"></i>
                        Dashboard
                    </a>
                </li>
                <li class="app-sidebar__heading">Manage</li>
                <li>
                    <a href="#">
                        <i class="metismenu-icon pe-7s-users"></i>
                        Client
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul>
                        <li>
                            <a href="add_client.php">
                                <i class="metismenu-icon pe-7s-add-user"></i>
                                
                                Add Clients 
                            </a>
                        </li>
                        <li>
                            <a href="manage_clients.php">
                                <i class="metismenu-icon">
                                </i>Manage Client
                            </a>
                        </li>
                       

                    </ul>
                </li>
                <li>
                    <a href="#">
                        <i class="metismenu-icon pe-7s-box2"></i>
                        Product
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul>
                        <li>
                            <a href="add_product.php">
                                <i class="metismenu-icon">
                                </i>Add Product
                            </a>
                        </li>
                        <li>
                            <a href="add_product.php">
                                <i class="metismenu-icon">
                                </i>Manage Product
                            </a>
                        </li>

                    </ul>
                </li>
                <li class="app-sidebar__heading">Invoice</li>
                <li>
                    <a href="#">
                        <i class="metismenu-icon pe-7s-note2"></i>
                       Invoice
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul>
                        <li>
                            <a href="gst_invoice.php">
                                <i class="metismenu-icon">
                                </i>GST Invoice
                            </a>
                        </li>
                        <li>
                            <a href="simple-invoice.php">
                                <i class="metismenu-icon">
                                </i>Simple invoice
                            </a>
                        </li>
                        <li>
                            <a href="delevery_challan.php">
                                <i class="metismenu-icon">
                                </i>Delevery Challan
                            </a>
                        </li>

                    </ul>
                </li>
                <li>
                    <a href="tables-regular.html">
                        <i class="metismenu-icon pe-7s-display2"></i>
                       History
                    </a>
                </li>
               
                <li class="app-sidebar__heading">Sales</li>
                <li>
                    <a href="sales.php">
                        <i class="metismenu-icon pe-7s-graph3">
                        </i>Sales
                    </a>
                </li>
               
                <li>
                    <a href="order.php">
                        <i class="metismenu-icon pe-7s-shopbag">
                        </i>Order
                    </a>
                </li>
                <li class="app-sidebar__heading">Payment</li>
                <li>
                    <a href="Payment-in.php">
                        <i class="metismenu-icon pe-7s-wallet">
                        </i>Payment-In
                    </a>
                </li>
                <li>
                    <a href="transection.php">
                    
                        <i class="metismenu-icon pe-7s-refresh-2">
                        </i>Transection
                    </a>
                </li>
                
                <li class="app-sidebar__heading">Report</li>
                <li>
                    <a href="#">
                        <i class="metismenu-icon pe-7s-car"></i>
                        Report
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul>
                        <li>
                            <a href="add_product.php">
                                <i class="metismenu-icon">
                                </i>Sale Report
                            </a>
                        </li>
                        <li>
                            <a href="components-accordions.html">
                                <i class="metismenu-icon">
                                </i>Product Report
                            </a>
                        </li>

                    </ul>
                </li>             
                
                
            </ul>
        </div>
    </div>
</div>